# Python Bokeh Management Dashboard 

---
Implement a bokeh server to visualize **_flights.csv_** dataset

### Three tabs were implemented
* a histogram tab
* a table tab
* a route tab

---
# Run Code
* install requirements
* open a cmd and type:
  * bokeh serve --show main.py

# 
### To change the dataset just change the directory and parameters on main module